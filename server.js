const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const express = require("express");
const admin = require("firebase-admin");
const cors = require("cors");

const serviceAccount = require("./serviceAccountKey.json");

function attachCsrfToken(url, cookie, value) {
  return function (req, res, next) {
    if (req.url == url) {
      console.log(`attachCsrfToken ${cookie} ${value}`);
      const options = {
        domain: "senestia.com",
      };
      res.cookie(cookie, value, options);
    }
    next();
  };
}

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const PORT = 3000;
const app = express();

app.use(
  cors({
    origin: [
      "http://localhost:3000",
      "http://localhost:3002",
      "https://poc-login1.senestia.com",
      "https://poc-login2.senestia.com",
    ],
    credentials: true,
  })
);
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(cookieParser());

app.use(
  attachCsrfToken(
    "/",
    "csrfToken",
    (Math.random() * 100000000000000000).toString()
  )
);

app.use("/", express.static("public"));

app.get("/", (req, res) => {
  res.send(JSON.stringify({ status: "success" }));
});

app.get("/verify-session", (req, res) => {
  const sessionCookie = req.cookies.session || "";
  console.log(`sessionCookie ${sessionCookie}`);
  admin
    .auth()
    .verifySessionCookie(sessionCookie, true /** checkRevoked */)
    .then((userData) => {
      const uid = userData.uid;
      admin
        .auth()
        .createCustomToken(uid)
        .then((customToken) => {
          console.log(`customToken ${customToken}`);
          res.set("Content-Type", "application/json");
          res.send(
            JSON.stringify({ status: "success", customToken: customToken })
          );
        })
        .catch((error) => {
          console.log(`error155 ${error}`);
          res.status(401).send("UNAUTHORIZED REQUEST!");
        });
    })
    .catch((error) => {
      console.log(`error160 ${error}`);
      res.status(401).send("UNAUTHORIZED REQUEST!");
    });
});

app.post("/login-session", (req, res) => {
  const idToken = req.body.idToken.toString();
  const csrfToken = req.body.csrfToken?.toString();

  const serverCsrfToken = req.cookies.csrfToken?.toString();

  // Guard against CSRF attacks.
  if (!req.cookies || csrfToken !== serverCsrfToken) {
    console.log(
      `Guard against CSRF attacks ${csrfToken} vs ${serverCsrfToken}`
    );
    res.status(401).send("UNAUTHORIZED REQUEST!");
    return;
  }

  const expiresIn = 60 * 60 * 24 * 5 * 1000; // 5 day

  admin
    .auth()
    .verifyIdToken(idToken)
    .then((decodedClaims) => {
      // In this case, we are enforcing that the user signed in in the last 5 minutes.
      if (new Date().getTime() / 1000 - decodedClaims.auth_time < 5 * 60) {
        return admin
          .auth()
          .createSessionCookie(idToken, { expiresIn: expiresIn });
      }
      throw new Error("Session expired");
    })
    .then((sessionCookie) => {
      // Note httpOnly cookie will not be accessible from javascript.
      // secure flag should be set to true in production
      const options = {
        maxAge: expiresIn,
        httpOnly: true,
        secure: true,
        domain: "senestia.com",
        sameSite: "strict",
      };
      console.log(`Set session Cookie ${sessionCookie}`);
      res.cookie("session", sessionCookie, options);
      res.end(JSON.stringify({ status: "success" }));
    })
    .catch((error) => {
      res.status(401).send("UNAUTHORIZED REQUEST!");
    });
});

app.get("/logout", (req, res) => {
  // Clear cookie.
  const sessionCookie = req.cookies.session || "";
  console.log(`logout sessionCookie ${sessionCookie}`);
  // Revoke session too. Note this will revoke all user sessions.
  admin
    .auth()
    .verifySessionCookie(sessionCookie, true)
    .then(function (decodedClaims) {
      return admin.auth().revokeRefreshTokens(decodedClaims.sub);
    })
    .then(function () {
      // Redirect to login page on success.
      const options = {
        httpOnly: true,
        secure: true,
        domain: "senestia.com",
        sameSite: "strict",
      };
      console.log(`clearCookie`);
      res.clearCookie("session", options);
      console.log(`res.status(200)`);
      res.status(200).end(JSON.stringify({ status: "success" }));
    })
    .catch(function () {
      // Session not valid Redirect to login page on error.
      console.log(`failed catch(function`);
      res.status(401).send("UNAUTHORIZED REQUEST!");
    });
});

app.listen(PORT, () => {
  console.log(`Sample app listening on port ${PORT}!`);
});

module.exports = app;
