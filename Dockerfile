FROM node:lts-alpine
ENV NODE_ENV=production
WORKDIR /my-project
COPY package.json yarn.lock ./
RUN apk add --no-cache yarn
RUN yarn install --production --silent && mv node_modules ../
COPY . .
EXPOSE 3000
RUN chown -f -R node /my-project
USER node
CMD ["yarn", "dev"]